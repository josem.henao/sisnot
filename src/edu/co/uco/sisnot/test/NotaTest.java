package edu.co.uco.sisnot.test;

import edu.co.uco.sisnot.domain.Asignatura;
import edu.co.uco.sisnot.domain.Nota;
import edu.co.uco.sisnot.domain.TipoNota;
import edu.co.uco.sisnot.exceptions.ValorNegativoException;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

public class NotaTest {

    private static final double NOTA_MINIMA = 3.0;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void calcularDefinitivaConNotasParaAprobar() throws ValorNegativoException {

        List<Nota> notas = new ArrayList<>();

        // Insertando una primera Nota
        Nota parcial  = new Nota();
        parcial.setTipoNota(new TipoNota(1, "PARCIAL", 0.3));
        parcial.setValor(4);
        notas.add(parcial);

        // Insertando una segunda Nota
        Nota notaFinal  = new Nota();
        notaFinal.setTipoNota(new TipoNota(2, "FINAL", 0.3));
        notaFinal.setValor(3);
        notas.add(notaFinal);

        // Insertando una tercera Nota
        Nota seguimiento  = new Nota();
        seguimiento.setTipoNota(new TipoNota(3, "SEGUIMIENTO", 0.4));
        seguimiento.setValor(5);
        notas.add(seguimiento);

        Asignatura ingenieriaSoftware = new Asignatura();
        ingenieriaSoftware.setNotas(notas);
        ingenieriaSoftware.setNotaMinima(NOTA_MINIMA);

        double resultadoEsperado = 4.1;
        double resultadoObtenido = ingenieriaSoftware.getDefinitiva();
        assertEquals(resultadoEsperado, resultadoObtenido,0);

    }

    @Test
    public void calcularDefinitivaConNotasParaReprobar() throws ValorNegativoException {

        List<Nota> notas = new ArrayList<>();

        // Insertando una primera Nota
        Nota parcial  = new Nota();
        parcial.setTipoNota(new TipoNota(1, "PARCIAL", 0.3));
        parcial.setValor(2);
        notas.add(parcial);

        // Insertando una segunda Nota
        Nota notaFinal  = new Nota();
        notaFinal.setTipoNota(new TipoNota(2, "FINAL", 0.3));
        notaFinal.setValor(1);
        notas.add(notaFinal);

        // Insertando una tercera Nota
        Nota seguimiento  = new Nota();
        seguimiento.setTipoNota(new TipoNota(3, "SEGUIMIENTO", 0.4));
        seguimiento.setValor(1);
        notas.add(seguimiento);

        Asignatura ingenieriaSoftware = new Asignatura();
        ingenieriaSoftware.setNotas(notas);
        ingenieriaSoftware.setNotaMinima(NOTA_MINIMA);

        double resultadoEsperado = 1.3;
        double resultadoObtenido = ingenieriaSoftware.getDefinitiva();
        assertEquals(resultadoEsperado, resultadoObtenido,0.01);

    }

    @Test
    public void verificarAsignaturaReprobada() throws ValorNegativoException {

        List<Nota> notas = new ArrayList<>();
        Asignatura ingenieriaSoftware = new Asignatura();

        // Insertando una primera Nota
        Nota parcial  = new Nota();
        parcial.setTipoNota(new TipoNota(1, "PARCIAL", 0.3));
        parcial.setValor(2);
        notas.add(parcial);

        // Insertando una segunda Nota
        Nota notaFinal  = new Nota();
        notaFinal.setTipoNota(new TipoNota(2, "FINAL", 0.3));
        notaFinal.setValor(1);
        notas.add(notaFinal);

        // Insertando una tercera Nota
        Nota seguimiento  = new Nota();
        seguimiento.setTipoNota(new TipoNota(3, "SEGUIMIENTO", 0.4));
        seguimiento.setValor(1);
        notas.add(seguimiento);

        ingenieriaSoftware.setNotas(notas);
        ingenieriaSoftware.setNotaMinima(NOTA_MINIMA);

        assertTrue(!ingenieriaSoftware.aprobo());

    }

    @Test
    public void validarNotasNegativas() throws ValorNegativoException{
        Nota nota = new Nota();
        exception.expect(ValorNegativoException.class);
        exception.expectMessage("No se admiten valores negativos");
        nota.setValor(-1);
    }

}