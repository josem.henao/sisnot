package edu.co.uco.sisnot.domain;

public class TipoNota {

    private int id;
    private String nombre;
    private double porcentaje;

    public TipoNota(int id, String nombre, double porcentaje) {
        super();
        this.id = id;
        this.nombre = nombre;
        this.porcentaje = porcentaje;
    }

    public TipoNota() {
        super();
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public double getPorcentaje() {
        return porcentaje;
    }
    public void setPorcentaje(double porcentaje) {
        this.porcentaje = porcentaje;
    }


}