package edu.co.uco.sisnot.domain;
import java.util.List;

public class Estudiante extends Persona{
    private List<Asignatura> asignaturas;

    public List<Asignatura> getAsignaturas() {
        return asignaturas;
    }

    public void setAsignaturas(List<Asignatura> asignaturas) {
        this.asignaturas = asignaturas;
    }
}
