package edu.co.uco.sisnot.domain;

import edu.co.uco.sisnot.exceptions.ValorNegativoException;

import javax.xml.validation.Validator;

public class Nota {
    private TipoNota tipoNota;
    private Asignatura asignatura;
    private Estudiante estudiante;
    private double valor;

    public TipoNota getTipoNota() {
        if (tipoNota==null) {
            tipoNota = new TipoNota();
        }
        return tipoNota;
    }

    public Asignatura getAsignatura() {
        if (asignatura==null) {
            asignatura = new Asignatura();
        }
        return asignatura;
    }

    public Estudiante getEstudiante() {
        if (estudiante==null) {
            estudiante = new Estudiante();
        }
        return estudiante;
    }

    public double getValor() {
        return valor;
    }

    public void setTipoNota(TipoNota tipoNota) {
        this.tipoNota = tipoNota;
    }

    public void setValor(double valor) throws ValorNegativoException {
        validarValor(valor);
        this.valor = valor;
    }

    private void validarValor(double valor) throws ValorNegativoException {
        if (valor < 0) {
            throw new ValorNegativoException("No se admiten valores negativos");
        }
    }
}
