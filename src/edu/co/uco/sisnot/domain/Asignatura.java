package edu.co.uco.sisnot.domain;

import java.util.List;

public class Asignatura {
    private String nombre;
    private Programa programa;
    private List<Nota> notas;
    private double notaMinima;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Programa getPrograma() {
        return programa;
    }

    public void setPrograma(Programa programa) {
        this.programa = programa;
    }

    public List<Nota> getNotas() {
        return notas;
    }

    public void setNotas(List<Nota> notas) {
        this.notas = notas;
    }

    public double getNotaMinima() {
        return notaMinima;
    }

    public void setNotaMinima(double notaMinima) {
        this.notaMinima = notaMinima;
    }

    public double getDefinitiva() {
        double definitiva = 0;
        for(Nota nota : notas) {
            definitiva = definitiva + (nota.getValor()*nota.getTipoNota().getPorcentaje());
        }
        return definitiva;
    }

    public boolean aprobo(){
        return getDefinitiva() >= notaMinima;
    }
}
